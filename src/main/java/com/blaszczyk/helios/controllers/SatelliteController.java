package com.blaszczyk.helios.controllers;

import com.blaszczyk.helios.entities.Satellite;
import com.blaszczyk.helios.services.SatelliteService;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/satellite")
public class SatelliteController {

    @Autowired
    private SatelliteService satelliteService;

    @GetMapping("")
    public List<Satellite> getAllSatellite() {
        return satelliteService.getAllSatellite();
    }

    @GetMapping("/{id}")
    public Optional<Satellite> getSatellite(@PathVariable Integer id) {
        return satelliteService.getSatellite(id);
    }

    @GetMapping("/by-planet/{id}")
    public List<Satellite> getAllSatellitesByPlanetId(@PathVariable Integer id) {
        return satelliteService.getAllSatellitesByPlanetId(id);
    }

    @GetMapping("/by-name/{name}")
    public Satellite findByName(@PathVariable String name){
        return satelliteService.findByName(name);
    }

    @PostMapping("/by-planet/{id}")
    public void addSatellite(@RequestBody Satellite satellite, @PathVariable Integer id) throws NotFoundException {
        satelliteService.addSatellite(satellite, id);
    }

    @PutMapping("/by-planet/{id}")
    public void updateSatellite(@RequestBody Satellite satellite, @PathVariable Integer id) throws NotFoundException {
        satelliteService.updateSatellite(satellite, id);
    }

    @DeleteMapping("/{id}")
    public void deleteSatellite(@PathVariable Integer id) {
        satelliteService.deleteSatellite(id);
    }






    @GetMapping("/by-planet/greater/{number}")
    public List<Satellite> findAllWherePlanetIdGreaterThan(@PathVariable Integer number){
        return satelliteService.findAllWherePlanetIdGreaterThan(number);
    }

}
