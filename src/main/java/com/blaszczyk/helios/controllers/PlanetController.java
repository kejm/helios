package com.blaszczyk.helios.controllers;

import com.blaszczyk.helios.dtos.PlanetDto;
import com.blaszczyk.helios.entities.Planet;
import com.blaszczyk.helios.services.PlanetService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@NoArgsConstructor
public class PlanetController {

    @Autowired
    private PlanetService planetService;

    @RequestMapping("/planet")
    public List<Planet> getAllPlanet() {
        return planetService.getAllPlanet();
    }

    @GetMapping("/planet/{id}")
    public Planet getPlanetById(@PathVariable Integer id) {
        return planetService.getPlanet(id);
    }

    @GetMapping("/planet/dto/{id}")
    public PlanetDto getPlanetDto(@PathVariable Integer id){
        return planetService.getPlanetDto(id);
    }

    @PostMapping(value = "/planet")
    public void addPlanet(@RequestBody Planet planet) {
        planetService.addPlanet(planet);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/planet/{id}")
    public void updatePlanet(@RequestBody Planet planet, @PathVariable Integer id) {
        planetService.updatePlanet(planet, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/planet/{id}")
    public void deletePlanet(@PathVariable Integer id) {
        planetService.deletePlanet(id);
    }







    @GetMapping("/planet/by-name/{name}")
    public Planet getPlanetByName(@PathVariable String name) {
        return planetService.getPlanetByName(name);
    }

    @GetMapping("/planet/by-system/{planetSystem}")
    public List<Planet> getAllPlanetFromSystem(@PathVariable String planetSystem){
        return planetService.getAllPlanetFromSystem(planetSystem);
    }

    @GetMapping("/planet/exoplanet")
    public List<Planet> getAllExoplanet(){
        return planetService.getAllExoplanet();
    }


    @PutMapping("/planet/updateEarth/{description}")
    public void updateDescriptionEarth(@PathVariable String description) {
        planetService.updateDescriptionEarth(description);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/planets/deletesaturn")
    public void deleteSaturn() {
        planetService.deleteSaturn();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/planets/dto")
    public void addPlanet(@RequestBody PlanetDto planetDto) {
        planetService.addPlanetDto(planetDto);
    }

    @GetMapping("/planet/simple")
    public List<Planet> getAllPlanetSimpleData(){
        return planetService.getAllPlanetSimpleData();
    }




    @RequestMapping({"/path/{id}", "/path"})
    @ResponseBody
    public String pathVariable(
            @PathVariable(required = false) Integer id) {
        return String.format("Wartość zmiennej id = %s", id);
    }

    @RequestMapping("/valuetest")
    @ResponseBody
    public String test1(
            @RequestParam(name = "planet1") String p1,
            @RequestParam(required = false, name = "planet2") String p2,
            @RequestParam int p3) {
        return String.format("Otrzymane wartości: planet1=%s, planet2=%s, p3=%d", p1, p2, p3);
    }

}
