package com.blaszczyk.helios.repositories;

import com.blaszczyk.helios.entities.Planet;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PlanetRepository extends CrudRepository<Planet, Integer> {

    Planet getPlanetById(Integer id);

    Planet getPlanetByName(String name);

    @Query(value = "SELECT * FROM planet WHERE planet_System = :planetSystem", nativeQuery = true)
    @Modifying
    @Transactional
    List<Planet> getAllPlanetFromSystem(@Param("planetSystem") String planetSystem);

    @Query(value = "SELECT * FROM planet WHERE type = 'exoplanet'", nativeQuery = true)
    List<Planet> getAllExoplanet();

    @Query(value = "UPDATE planet SET description = :description WHERE name = 'Earth'", nativeQuery = true)
    @Modifying
    @Transactional
    void updateDescriptionEarth(@Param("description") String description);

    @Query(value = "DELETE planet FROM planet WHERE name = 'Saturn'", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteSaturn();

    @Query(value = "SELECT id,name,description,planet_system,constellation,type,mass,star,escape_velocity FROM helios.planet", nativeQuery = true)
    List<Planet> getAllPlanetSimpleData();

}

