package com.blaszczyk.helios.repositories;

import com.blaszczyk.helios.entities.Satellite;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SatelliteRepository extends CrudRepository<Satellite, Integer> {

    List<Satellite> findByPlanetId(Integer planetId);

    List<Satellite> findAllByPlanetIdGreaterThan(Integer number);

    Satellite findByName(String name);

    
}
