package com.blaszczyk.helios.services;

import com.blaszczyk.helios.dtos.PlanetDto;
import com.blaszczyk.helios.entities.Planet;
import com.blaszczyk.helios.mappers.PlanetMapper;
import com.blaszczyk.helios.repositories.PlanetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlanetService {

    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private PlanetMapper planetMapper;

    public List<Planet> getAllPlanet() {
        List<Planet> planets = new ArrayList<>();
        planetRepository.findAll().forEach(planets::add);
        return planets;
    }

    public Planet getPlanet(Integer id) {
        return planetRepository.getPlanetById(id);
    }

    public Planet getPlanetByName(String name) {
        return planetRepository.getPlanetByName(name);
    }

    public void addPlanet(Planet planet) {
        planetRepository.save(planet);
    }

    public void updatePlanet(Planet planet, Integer id) {
        planetRepository.save(planet);
    }

    public void deletePlanet(Integer id) {
        planetRepository.deleteById(id);
    }



    public List<Planet> getAllPlanetFromSystem(String planetSystem) {
        List<Planet> planets = new ArrayList<>();
        planetRepository.getAllPlanetFromSystem(planetSystem).forEach(planets::add);
        return planets;
    }

    public List<Planet> getAllExoplanet() {
        List<Planet> planets = new ArrayList<>();
        planetRepository.getAllExoplanet().forEach(planets::add);
        return planets;
    }

    public void updateDescriptionEarth(String description) {
        planetRepository.updateDescriptionEarth(description);
    }

    public void deleteSaturn() {
        planetRepository.deleteSaturn();
    }

    public void addPlanetDto(PlanetDto planetDto) {
        Planet planet = planetMapper.toPlanet(planetDto);
        planetRepository.save(planet);
    }

    public List<Planet> getAllPlanetSimpleData() {
        List<Planet> planets = new ArrayList<>();
        planetRepository.getAllPlanetSimpleData().forEach(planets::add);
        return planets;
    }

    public PlanetDto getPlanetDto(Integer id) {
        return planetMapper.toDto(planetRepository.findById(id).get());
    }
}
