package com.blaszczyk.helios.services;

import com.blaszczyk.helios.entities.Planet;
import com.blaszczyk.helios.entities.Satellite;
import com.blaszczyk.helios.repositories.PlanetRepository;
import com.blaszczyk.helios.repositories.SatelliteRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SatelliteService {

    @Autowired
    private SatelliteRepository satelliteRepository;

    @Autowired
    private PlanetRepository planetRepository;

    public List<Satellite> getAllSatellite(){
        List<Satellite> satellites = new ArrayList<>();
        satelliteRepository.findAll().forEach(satellites::add);
        return satellites;
    }

    public List<Satellite> getAllSatellitesByPlanetId(Integer planetId){
        List<Satellite> satellites = new ArrayList<>();
        satelliteRepository.findByPlanetId(planetId).forEach(satellites::add);
        return satellites;
    }

    public Optional<Satellite> getSatellite(Integer id) {
        return satelliteRepository.findById(id);
    }

    public void addSatellite(Satellite satellite, Integer id) throws NotFoundException {
        Optional<Planet> planet = planetRepository.findById(id);
        Planet planetEntity;
        if(planet.isPresent()){
            planetEntity = planet.get();
            satellite.setPlanet(planetEntity);
            satelliteRepository.save(satellite);
        } else {
            throw new NotFoundException("PLANET NOT FOUND! ");
        }
    }

    public void updateSatellite(Satellite satellite, Integer id) throws NotFoundException {
        Optional<Planet> planet = planetRepository.findById(id);
        Planet planetEntity;
        if(planet.isPresent()){
            planetEntity = planet.get();
            satellite.setPlanet(planetEntity);
            satelliteRepository.save(satellite);
        } else {
            throw new NotFoundException("Not found planet to edit");
        }
    }

    public void deleteSatellite(Integer id) {
        satelliteRepository.deleteById(id);
    }

    public List<Satellite> findAllWherePlanetIdGreaterThan(int i) {
        return satelliteRepository.findAllByPlanetIdGreaterThan(i);
    }

    public Satellite findByName(String name) {
        return satelliteRepository.findByName(name);
    }

}
