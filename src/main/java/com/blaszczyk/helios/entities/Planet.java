package com.blaszczyk.helios.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Planet extends AbstractEntity {

    private String name;
    private String description;
    private String planetSystem;
    private String constellation;
    private String type;

    private float mass;
    private String star;
    private float escapeVelocity;

}
