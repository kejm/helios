package com.blaszczyk.helios.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class Satellite extends AbstractEntity {

    private String name;
    private String description;
    private float mass;

    @ManyToOne
    @JoinColumn(name = "planet_id")
    private Planet planet;

    public void setPlanet(Planet planet) {
        this.planet = planet;
    }

}
