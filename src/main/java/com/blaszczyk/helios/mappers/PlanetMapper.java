package com.blaszczyk.helios.mappers;

import com.blaszczyk.helios.dtos.PlanetDto;
import com.blaszczyk.helios.entities.Planet;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;


@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PlanetMapper {
    Planet toPlanet(PlanetDto planetDto);
    PlanetDto toDto(Planet planet);
}
